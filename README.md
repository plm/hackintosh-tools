Hackintosh Tools
===

Some helpful tools when testing your Apple or Hackintosh hardware.

Examples
---

Normalize `ioreg` output and use `diff` to look for changes between macOS
versions or [Clover][1]/[OpenCore][2] configurations. Host-specific attributes
and values are redacted so the resulting output can be safety shared without
risk of compromising sensitive information.

```shell
ioreg -l -w0 | ./normalize-ioreg.py | sort > today.out
diff -u yesterday.out today.out | less
```

Since `ioreg` displays information about your attached devices, if a name
includes non-UTF-8 characters, such as ISO-8859-1's copyright symbol (`0xa8`),
you will encounter a `UnicodeDecodeError` if UTF-8 is assumed for `STDIN`. As
a workaround, [explicitly set the encoding][3] used for I/O.

```shell
PYTHONIOENCODING=latin_1 ./normalize-ioreg.py
```

[1]: https://github.com/CloverHackyColor/CloverBootloader
[2]: https://github.com/acidanthera/OpenCorePkg
[3]: https://docs.python.org/3/library/sys.html#sys.stdin
