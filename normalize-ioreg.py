#!/usr/bin/env python3

import sys
import re

parents = []
seen_paths = {}
qualifier = ""
drop_ids = True
drop_busy = True
drop_retain = True
redact_pids = True
redact_user = True
redact_smbios = True
redact_ahci_serials = True

i = 1
for line in sys.stdin:
    if not line:
        continue
    # Always strip first two characters ("+-" or "  ")
    indents = [line[:2]]
    rest = line[2:].rstrip("\n")
    try:
        for _ in parents:
            # Nibble off the indentation for each parent
            indent = rest[:2]
            if indent in ["o "]:
                # New object, stop nibbling
                break
            else:
                indents.append(indent)
                rest = rest[2:]
        # Number of parents should match number of nibbles
        parents = parents[:len(indents)-1]
        if len(rest) > 2:
            if rest.startswith("o "):
                rest = rest[2:]
                # New object
                name, data = rest.rstrip().split("  ", 1)
                if drop_ids:
                    data = re.sub(", id 0x[a-f0-9]+,", ",", data, 1)
                if drop_busy:
                    data = re.sub(", busy [0-9]+( \\([0-9]+ ms\\))?,", ",", data, 1)
                if drop_retain:
                    data = re.sub(", retain [0-9]+", "", data, 1)
                parents.append(name)
                path = "/".join(parents)
                if path in seen_paths:
                    count = seen_paths[path]+1
                    seen_paths[path] = count
                    if count > 0:
                        qualifier = "#%02x" % count
                else:
                    seen_paths[path] = 0
                    qualifier = ""
                print(path + qualifier, data)
            elif rest == "{" or rest == "}":
                # Object props start or end; skip line
                continue
            elif rest.startswith("  "):
                # Property
                key, value = rest[2:].split("=", 1)
                key = key.strip(" \"")
                if redact_pids:
                    if key == "pid":
                        value = "?"
                    else:
                        value = re.sub("pid [0-9]+,", "pid ?,", value, 1)
                if redact_user:
                    value = re.sub("(\"kCGSession(Long)?UserNameKey\"=)\"[^\"]+\"", "\\1\"?\"", value, 1)
                if redact_smbios:
                    if len(parents) == 2:
                        if key in ["board-id", "IOPlatformUUID", "IOPlatformSerialNumber"]:
                            value = re.sub("\"[^\"]+\"", "\"?\"", value, 1)
                if redact_ahci_serials:
                    if "AppleAHCIDiskDriver" in parents or "IOAHCISerialATAPI" in parents:
                        if key == "Serial Number":
                            value = "\"?\""
                        else:
                            value = re.sub("(\"Serial Number\"=)\"[^\"]+\"", "\\1\"?\"", value, 1)
                print("/".join(parents) + qualifier, key, "=", value.strip())
            else:
                assert False
        i = i+1
    except Exception as e:
        print("Error", e, "on line", i, "within", "/".join(parents), file=sys.stderr)
        print(line.rstrip(), file=sys.stderr)
        print(("  " * len(indents)) + "^", file=sys.stderr)
        print(("  " * len(indents)) + "|", file=sys.stderr)
        print(("  " * len(indents)) + "`- Unexpected input", file=sys.stderr)
        exit(1)
